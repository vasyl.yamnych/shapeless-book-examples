name := "shapeless-book-examples"

version := "0.1"

scalaVersion := "2.12.8"

scalacOptions += "-Ypartial-unification"

libraryDependencies ++= Seq(
  "com.chuusai" %% "shapeless" % "2.3.3",
  "org.typelevel" %% "cats-core" % "1.5.0",
  "org.scalacheck" %% "scalacheck" % "1.14.0" // % "test"
)