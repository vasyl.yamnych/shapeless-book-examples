import shapeless._
import shapeless.record._

case class IceCream(name: String, numCherries: Int, inCone: Boolean)

val sundae = LabelledGeneric[IceCream].to(IceCream("Sundae", 1, false))

sundae.get('name)

sundae.get('numCherries)

//sundae.get('nomCherries)

val sundaeUpdated = sundae.updated('numCherries, 3)
val sundaeRemove = sundae.remove('inCone)

val sundaeWith = sundae.updateWith('name)("MASSIVE " + _)
sundae.toMap

//Type of numCherries actually is not changed
val sundaeChangeType = sundae.updated('numCherries, "changeType")

case class Address(street: String)
case class Person(name: String, address: Address)

val person = Person("John", Address("NY 5 av"))

val personRepr = LabelledGeneric[Person].to(person)
personRepr.get('address)
personRepr.get('address).street