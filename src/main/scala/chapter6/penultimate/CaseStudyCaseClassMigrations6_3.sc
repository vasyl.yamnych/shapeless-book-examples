case class IceCreamV1(name: String, numCherries: Int, inCone: Boolean)

// Remove fields:
case class IceCreamV2a(name: String, inCone: Boolean)

// Reorder fields:
case class IceCreamV2b(name: String, inCone: Boolean, numCherries: Int)

// Insert fields (provided we can determine a default value):
case class IceCreamV2c(name: String, inCone: Boolean, numCherries: Int, numWaffles: Int)


trait Migration[A, B] {
  def apply(a: A): B
}

implicit class MigrationOps[A](a: A) {
  def migrateTo[B](implicit migration: Migration[A, B]): B =
    migration.apply(a)
}

import shapeless._
import shapeless.ops.hlist

//implicit def genericMigration[A, B, ARepr <: HList, BRepr <: HList](
//                                                                     implicit
//                                                                     aGen: LabelledGeneric.Aux[A, ARepr],
//                                                                     bGen: LabelledGeneric.Aux[B, BRepr],
//                                                                     inter: hlist.Intersection.Aux[ARepr, BRepr, BRepr]
//                                                                   ): Migration[A, B] = new Migration[A, B] {
//  def apply(a: A): B =
//    bGen.from(inter.apply(aGen.to(a)))
//}

IceCreamV1("Sundae", 1, false).migrateTo[IceCreamV2a]
//IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2b]

//implicit def genericMigration[A, B, ARepr <: HList, BRepr <: HList, Unaligned <: HList](
//                                                                                         implicit
//                                                                                         aGen: LabelledGeneric.Aux[A, ARepr],
//                                                                                         bGen: LabelledGeneric.Aux[B, BRepr],
//                                                                                         inter: hlist.Intersection.Aux[ARepr, BRepr, Unaligned],
//                                                                                         align: hlist.Align[Unaligned, BRepr]
//                                                                                       ): Migration[A, B] = new Migration[A, B] {
//  def apply(a: A): B =
//    bGen.from(align.apply(inter.apply(aGen.to(a))))
//}

IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2a]
IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2b]
//IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2c]

import cats.Monoid
import cats.instances.all._
import shapeless.labelled.{field, FieldType}

def createMonoid[A](zero: A)(add: (A, A) => A): Monoid[A] =
  new Monoid[A] {
    def empty = zero

    def combine(x: A, y: A): A = add(x, y)
  }

implicit val hnilMonoid: Monoid[HNil] =
  createMonoid[HNil](HNil)((x, y) => HNil)

implicit def emptyHList[K <: Symbol, H, T <: HList](
                                                     implicit
                                                     hMonoid: Lazy[Monoid[H]],
                                                     tMonoid: Monoid[T]
                                                   ): Monoid[FieldType[K, H] :: T] =
  createMonoid(field[K](hMonoid.value.empty) :: tMonoid.empty) {
    (x, y) =>
      field[K](hMonoid.value.combine(x.head, y.head)) ::
        tMonoid.combine(x.tail, y.tail)
  }

implicit def genericMigration[A, B, ARepr <: HList, BRepr <: HList,
Common <: HList, Added <: HList, Unaligned <: HList](
                                                      implicit
                                                      aGen: LabelledGeneric.Aux[A, ARepr],
                                                      bGen: LabelledGeneric.Aux[B, BRepr],
                                                      inter: hlist.Intersection.Aux[ARepr, BRepr, Common],
                                                      diff: hlist.Diff.Aux[BRepr, Common, Added],
                                                      monoid: Monoid[Added],
                                                      prepend: hlist.Prepend.Aux[Added, Common, Unaligned],
                                                      align: hlist.Align[Unaligned, BRepr]
                                                    ): Migration[A, B] =
  new Migration[A, B] {
    def apply(a: A): B =
      bGen.from(align(prepend(monoid.empty, inter(aGen.to(a)))))
  }

IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2a]
IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2b]
IceCreamV1("Sundae", 1, true).migrateTo[IceCreamV2c]
