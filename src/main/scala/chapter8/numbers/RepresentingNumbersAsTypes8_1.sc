import shapeless.{Nat, Succ}

type Zero = Nat._0
type One = Succ[Zero]
type Two = Succ[One]
Nat._1
Nat._2
Nat._3

import shapeless.ops.nat.ToInt

val toInt = ToInt[Two]
toInt.apply()
Nat.toInt[Nat._3]