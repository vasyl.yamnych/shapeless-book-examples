import shapeless.ops.hlist.Last
import shapeless.{::, HList, HNil, the}

val last1 = Last[String :: Int :: HNil]
val last2 = Last[Int :: String :: HNil]

last1("foo" :: 123 :: HNil)
last2(321 :: "bar" :: HNil)

// Last[HNil]
// last1(321 :: "bar" :: HNil)

trait Second[L <: HList] {
  type Out
  def apply(value: L): Out
}

object Second {
  type Aux[L <: HList, O] = Second[L] { type Out = O }
  def apply[L <: HList](implicit inst: Second[L]): Aux[L, inst.Out] =
    inst
}

//object Second {
//  type Aux[L <: HList, O] = Second[L] { type Out = O }
//  def apply[L <: HList](implicit inst: Second[L]): Second[L] =
//    inst
//}


implicitly[Last[String :: Int :: HNil]]
Last[String :: Int :: HNil]
the[Last[String :: Int :: HNil]]

implicit def hlistSecond[A, B, Rest <: HList]: Second.Aux[A :: B :: Rest, B] =
  new Second[A :: B :: Rest] {
    type Out = B
    def apply(value: A :: B :: Rest): B =
      value.tail.head
  }

val second1 = Second[String :: Boolean :: Int :: HNil]

val second2 = Second[String :: Int :: Boolean :: HNil]

//Second[String :: HNil]

second1("foo" :: true :: 123 :: HNil)
second2("bar" :: 321 :: false :: HNil)

//second1("baz" :: HNil)
