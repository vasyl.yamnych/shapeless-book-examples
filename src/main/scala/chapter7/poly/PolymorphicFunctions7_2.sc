trait CaseFake[P, A] {
  type Result

  def apply(a: A): Result
}

trait PolyFake {
  def apply[A](arg: A)(implicit cse: CaseFake[this.type, A]): cse.Result =
    cse.apply(arg)
}

object myPolyFake extends PolyFake {
  implicit def intCase =
    new CaseFake[this.type, Int] {
      type Result = Double

      def apply(num: Int): Double = num / 2.0
    }

  implicit def stringCase =
    new CaseFake[this.type, String] {
      type Result = Int

      def apply(str: String): Int = str.length
    }
}

myPolyFake.apply(123)
myPolyFake.apply("hello")

trait CaseFake2[A] {
  type Result

  def apply(a: A): Result
}

trait PolyFake2 {
  def apply[A](arg: A)(implicit cse: CaseFake2[A]): cse.Result
}

object myPolyFake2 extends PolyFake2 {
  implicit def intCase =
    new CaseFake2[Int] {
      type Result = Double

      def apply(num: Int): Double = num / 2.0
    }

  implicit def stringCase =
    new CaseFake2[String] {
      type Result = Int

      def apply(str: String): Int = str.length
    }

  def apply[A](arg: A)(implicit cse: CaseFake2[A]): cse.Result = cse.apply(arg)

}

import myPolyFake2._
myPolyFake2.apply(123)
myPolyFake2.apply("hello")

import shapeless._

object myPoly extends Poly1 {
  implicit val intCase: Case.Aux[Int, Double] =
    at(num => num / 2.0)
  implicit val stringCase: Case.Aux[String, Int] =
    at(str => str.length)
}

myPoly.apply(123)
myPoly.apply("hello")

object multiply extends Poly2 {
  implicit val intIntCase: Case.Aux[Int, Int, Int] =
    at((a, b) => a * b)
  implicit val intStrCase: Case.Aux[Int, String, String] =
    at((a, b) => b * a)
}

multiply(3, 4)
multiply(3, "4")

import scala.math.Numeric

object total extends Poly1 {
  implicit def base[A](implicit num: Numeric[A]): Case.Aux[A, Double] =
    at(num.toDouble)

  implicit def option[A](implicit num: Numeric[A]): Case.Aux[Option[A], Double] =
    at(opt => opt.map(num.toDouble).getOrElse(0.0))

  implicit def list[A](implicit num: Numeric[A]): Case.Aux[List[A], Double] =
    at(list => num.toDouble(list.sum))
}

total(10)
total(Option(20.0))
total(List(1L, 2L, 3L))

val a = myPoly.apply(123)
//val a2: Double = myPoly.apply(123)
val a3: Double = myPoly.apply[Int](123)
