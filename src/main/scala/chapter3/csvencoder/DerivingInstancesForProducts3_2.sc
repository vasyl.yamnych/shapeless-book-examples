import chapter3.csvencoder.CsvEncoder._
import chapter3.csvencoder._
import chapter3.csvencoder.utils.TestData._
import chapter3.csvencoder.utils.Utils._
import shapeless.{::, Generic, HList, HNil}


implicit val stringEncoder: CsvEncoder[String] =
  createEncoder(str => List(str))
implicit val intEncoder: CsvEncoder[Int] =
  createEncoder(num => List(num.toString))
implicit val booleanEncoder: CsvEncoder[Boolean] =
  createEncoder(bool => List(if (bool) "yes" else "no"))

implicit val hnilEncoder: CsvEncoder[HNil] =
  createEncoder(hnil => { println("### hnilEncoder"); Nil })
implicit def hlistEncoder[H, T <: HList](
    implicit
    hEncoder: CsvEncoder[H],
    tEncoder: CsvEncoder[T]
): CsvEncoder[H :: T] = createEncoder {
  case h :: t =>
    hEncoder.encode(h) ++ tEncoder.encode(t)
}

val reprEncoder: CsvEncoder[String :: Int :: Boolean :: HNil] =
  implicitly

reprEncoder.encode("abc" :: 123 :: true :: HNil)

//val gen = Generic[IceCream]
//val enc = the[CsvEncoder[gen.Repr]]
//
//implicit val iceCreamEncoder: CsvEncoder[IceCream] = {
//  val gen = Generic[IceCream]
//  val enc = the[CsvEncoder[gen.Repr]]
//  createEncoder(iceCream => enc.encode(gen.to(iceCream)))
//}

//implicit def genericEncoder[A, R <: HList](
//    implicit
//    gen: Generic[A] { type Repr = R },
//    enc: CsvEncoder[R]
//): CsvEncoder[A] = createEncoder(a => enc.encode(gen.to(a)))

implicit def genericEncoder[A, R](
    implicit
    gen: Generic.Aux[A, R],
    enc: CsvEncoder[R]
): CsvEncoder[A] = createEncoder(a => enc.encode(gen.to(a)))


writeCsv(iceCreams)