package chapter3.csvencoder.model

case class Employee(name: String, number: Int, manager: Boolean)
