package chapter3.csvencoder.model

case class IceCream(name: String, numCherries: Int, inCone: Boolean)
