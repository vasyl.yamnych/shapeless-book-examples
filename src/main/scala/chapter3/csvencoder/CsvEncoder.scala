package chapter3.csvencoder

trait CsvEncoder[A] {
  def encode(value: A): List[String]
}

object CsvEncoder {
  def createEncoder[A](func: A => List[String]): CsvEncoder[A] =
    new CsvEncoder[A] {
      def encode(value: A): List[String] = func(value)
    }
}
