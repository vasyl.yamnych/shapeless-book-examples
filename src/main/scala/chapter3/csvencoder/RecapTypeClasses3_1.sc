import chapter3.csvencoder._
import chapter3.csvencoder.model.{Employee, IceCream}
import chapter3.csvencoder.utils.TestData._
import chapter3.csvencoder.utils.Utils._

import shapeless._

// CsvEncoder instance for the custom data type:
implicit val employeeEncoder: CsvEncoder[Employee] =
  new CsvEncoder[Employee] {
    def encode(e: Employee): List[String] =
      List(
        e.name,
        e.number.toString,
        if (e.manager) "yes" else "no"
      )
  }

writeCsv(employees)

implicit val iceCreamEncoder: CsvEncoder[IceCream] =
  new CsvEncoder[IceCream] {
    def encode(i: IceCream): List[String] =
      List(
        i.name,
        i.numCherries.toString,
        if (i.inCone) "yes" else "no"
      )
  }

writeCsv(iceCreams)

implicit def pairEncoder[A, B](
                                implicit
                                aEncoder: CsvEncoder[A],
                                bEncoder: CsvEncoder[B]
                              ): CsvEncoder[(A, B)] =
  new CsvEncoder[(A, B)] {
    def encode(pair: (A, B)): List[String] = {
      val (a, b) = pair
      aEncoder.encode(a) ++ bEncoder.encode(b)
    }
  }

employees zip iceCreams
writeCsv(employees zip iceCreams)

object CsvEncoder {
  // "Summoner" method
  def apply[A](implicit enc: CsvEncoder[A]): CsvEncoder[A] =
    enc

  // "Constructor" method
  def instance[A](func: A => List[String]): CsvEncoder[A] =
    new CsvEncoder[A] {
      def encode(value: A): List[String] =
        func(value)
    }

  // Globally visible type class instances
}

CsvEncoder[IceCream]

implicitly[CsvEncoder[IceCream]]

the[CsvEncoder[IceCream]]