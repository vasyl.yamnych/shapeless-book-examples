import chapter3.csvencoder.CsvEncoder
import chapter3.csvencoder.CsvEncoder._
import shapeless.{:+:, ::, CNil, Coproduct, Generic, HList, HNil, Inl, Inr, Lazy, the}

implicit val hnilEncoder: CsvEncoder[HNil] =
  createEncoder(hnil => {
    println("### hnilEncoder");
    Nil
  })
implicit val cnilEncoder: CsvEncoder[CNil] =
  createEncoder(cnil => throw new Exception("Inconceivable!"))
implicit val intEncoder: CsvEncoder[Int] =
  createEncoder(num => List(num.toString))

implicit def hlistEncoder[H, T <: HList](
                                          implicit
                                          hEncoder: Lazy[CsvEncoder[H]], // wrap in Lazy
                                          tEncoder: CsvEncoder[T]
                                        ): CsvEncoder[H :: T] = createEncoder {
  case h :: t =>
    hEncoder.value.encode(h) ++ tEncoder.encode(t)
}

implicit def coproductEncoder[H, T <: Coproduct](
                                                  implicit
                                                  hEncoder: Lazy[CsvEncoder[H]], // wrap in Lazy
                                                  tEncoder: CsvEncoder[T]
                                                ): CsvEncoder[H :+: T] = createEncoder {
  case Inl(h) => hEncoder.value.encode(h)
  case Inr(t) => tEncoder.encode(t)
}

implicit def genericEncoder[A, R](
                                   implicit
                                   gen: Generic.Aux[A, R],
                                   rEncoder: Lazy[CsvEncoder[R]] // wrap in Lazy
                                 ): CsvEncoder[A] = createEncoder { value =>
  rEncoder.value.encode(gen.to(value))
}

// ##### ##### #####

case class Foo(bar: Int, baz: Float)

//the[CsvEncoder[Foo]]
val foo = Foo(11, 3.14f)
val repr = Generic[Foo].to(foo)

//the[CsvEncoder[Int :: Float :: HNil]]

the[CsvEncoder[Int]]
//the[CsvEncoder[Float]]

implicit val floatEncoder: CsvEncoder[Float] =
  createEncoder(num => List(num.toString))

the[CsvEncoder[Float]]

the[CsvEncoder[Foo]]

val gen = Generic[Foo]
val enc = the[CsvEncoder[gen.Repr]]
