import chapter3.csvencoder.CsvEncoder
import chapter3.csvencoder.CsvEncoder._
import chapter3.csvencoder.model.Tree
import shapeless.{:+:, ::, CNil, Coproduct, Generic, HList, HNil, Inl, Inr, Lazy, the}

implicit val hnilEncoder: CsvEncoder[HNil] =
  createEncoder(hnil => {
    println("### hnilEncoder"); Nil
  })
implicit val cnilEncoder: CsvEncoder[CNil] =
  createEncoder(cnil => throw new Exception("Inconceivable!"))
implicit val intEncoder: CsvEncoder[Int] =
  createEncoder(num => List(num.toString))

implicit def hlistEncoder[H, T <: HList](
                                          implicit
                                          hEncoder: Lazy[CsvEncoder[H]], // wrap in Lazy
                                          tEncoder: CsvEncoder[T]
                                        ): CsvEncoder[H :: T] = createEncoder {
  case h :: t =>
    hEncoder.value.encode(h) ++ tEncoder.encode(t)
}

implicit def coproductEncoder[H, T <: Coproduct](
                                                  implicit
                                                  hEncoder: Lazy[CsvEncoder[H]], // wrap in Lazy
                                                  tEncoder: CsvEncoder[T]
                                                ): CsvEncoder[H :+: T] = createEncoder {
  case Inl(h) => hEncoder.value.encode(h)
  case Inr(t) => tEncoder.encode(t)
}

implicit def genericEncoder[A, R](
                                   implicit
                                   gen: Generic.Aux[A, R],
                                   rEncoder: Lazy[CsvEncoder[R]] // wrap in Lazy
                                 ): CsvEncoder[A] = createEncoder { value =>
  rEncoder.value.encode(gen.to(value))
}



the[CsvEncoder[Tree[Int]]]
implicitly[CsvEncoder[Tree[Int]]]
