package chapter3.csvencoder.utils

import chapter3.csvencoder.CsvEncoder

object Utils {

  def writeCsv[A](values: List[A])(implicit enc: CsvEncoder[A]): String =
    values.map(value => enc.encode(value).mkString(",")).mkString("\n")
}
