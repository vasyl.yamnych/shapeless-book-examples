import chapter3.csvencoder.CsvEncoder._
import chapter3.csvencoder._
import chapter3.csvencoder.utils.TestData._
import chapter3.csvencoder.utils.Utils._
import shapeless.{:+:, ::, CNil, Coproduct, Generic, HList, HNil, Inl, Inr}


implicit def genericEncoder[A, R](
                                   implicit
                                   gen: Generic.Aux[A, R],
                                   enc: CsvEncoder[R]
                                 ): CsvEncoder[A] = createEncoder(a => enc.encode(gen.to(a)))

implicit val hnilEncoder: CsvEncoder[HNil] =
  createEncoder(hnil => { println("### hnilEncoder"); Nil })
implicit def hlistEncoder[H, T <: HList](
                                          implicit
                                          hEncoder: CsvEncoder[H],
                                          tEncoder: CsvEncoder[T]
                                        ): CsvEncoder[H :: T] = createEncoder {
  case h :: t =>
    hEncoder.encode(h) ++ tEncoder.encode(t)
}

implicit val cnilEncoder: CsvEncoder[CNil] =
  createEncoder(cnil => throw new Exception("Inconceivable!"))

implicit def coproductEncoder[H, T <: Coproduct](
                                                  implicit hEncoder: CsvEncoder[H],
                                                  tEncoder: CsvEncoder[T]
                                                ): CsvEncoder[H :+: T] = createEncoder {
  case Inl(h) => hEncoder.encode(h)
  case Inr(t) => tEncoder.encode(t)
}


implicit val doubleEncoder: CsvEncoder[Double] =
  createEncoder(d => List(d.toString))


writeCsv(shapes)
