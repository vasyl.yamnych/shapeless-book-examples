package chapter5.jsonvalue.utils

import chapter5.jsonvalue.model._

object TestData {

  case class IceCream(name: String, numCherries: Int, inCone: Boolean)

  val iceCream = IceCream("Sundae", 1, false)

  // Ideally we'd like to produce something like this:
  val iceCreamJson: JsonValue =
    JsonObject(List(
      "name" -> JsonString("Sundae"),
      "numCherries" -> JsonNumber(1),
      "inCone" -> JsonBoolean(false)
    ))

  sealed trait Shape

  final case class Rectangle(width: Double, height: Double) extends Shape

  final case class Circle(radius: Double) extends Shape

}
