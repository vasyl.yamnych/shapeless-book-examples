import chapter5.jsonvalue.encoder.JsonEncoder
import chapter5.jsonvalue.model.JsonObject
import chapter5.jsonvalue.utils.TestData._
import shapeless.LabelledGeneric

val gen = LabelledGeneric[IceCream].to(iceCream)

trait JsonObjectEncoder[A] extends JsonEncoder[A] {
  def encode(value: A): JsonObject
}

def createObjectEncoder[A](fn: A => JsonObject): JsonObjectEncoder[A] =
  new JsonObjectEncoder[A] {
    def encode(value: A): JsonObject =
      fn(value)
  }

import shapeless.{HList, ::, HNil, Lazy}

implicit val hnilEncoder: JsonObjectEncoder[HNil] =
  createObjectEncoder(hnil => JsonObject(Nil))

//implicit def hlistObjectEncoder[H, T <: HList](
//                                                implicit
//                                                hEncoder: Lazy[JsonEncoder[H]],
//                                                tEncoder: JsonObjectEncoder[T]
//                                              ): JsonEncoder[H :: T] = ???

import shapeless.Witness
import shapeless.labelled.FieldType

//implicit def hlistObjectEncoder[K, H, T <: HList](
//                                                   implicit
//                                                   hEncoder: Lazy[JsonEncoder[H]],
//                                                   tEncoder: JsonObjectEncoder[T]
//                                                 ): JsonObjectEncoder[FieldType[K, H] :: T] = ???

//implicit def hlistObjectEncoder[K <: Symbol, H, T <: HList](
//                                                             implicit
//                                                             witness: Witness.Aux[K],
//                                                             hEncoder: Lazy[JsonEncoder[H]],
//                                                             tEncoder: JsonObjectEncoder[T]
//                                                           ): JsonObjectEncoder[FieldType[K, H] :: T] = {
//  val fieldName: String = witness.value.name
//  ???
//}

implicit def hlistObjectEncoder[K <: Symbol, H, T <: HList](
                                                             implicit
                                                             witness: Witness.Aux[K],
                                                             hEncoder: Lazy[JsonEncoder[H]],
                                                             tEncoder: JsonObjectEncoder[T]
                                                           ): JsonObjectEncoder[FieldType[K, H] :: T] = {
  val fieldName: String = witness.value.name
  createObjectEncoder { hlist =>
    val head = hEncoder.value.encode(hlist.head)
    val tail = tEncoder.encode(hlist.tail)
    JsonObject((fieldName, head) :: tail.fields)
  }
}

implicit def genericObjectEncoder[A, H](
                                         implicit
                                         generic: LabelledGeneric.Aux[A, H],
                                         hEncoder: Lazy[JsonObjectEncoder[H]]
                                       ): JsonEncoder[A] =
  createObjectEncoder { value =>
    hEncoder.value.encode(generic.to(value))
  }

import chapter5.jsonvalue.encoder.Implicits._
JsonEncoder[IceCream].encode(iceCream)
