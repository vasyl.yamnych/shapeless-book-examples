import chapter5.jsonvalue.encoder.JsonEncoder
import chapter5.jsonvalue.model.JsonObject
import chapter5.jsonvalue.utils.TestData.{Circle, Shape}
import shapeless.labelled.FieldType
import shapeless.{:+:, ::, CNil, Coproduct, HList, HNil, Inl, Inr, LabelledGeneric, Lazy, Witness}

LabelledGeneric[Shape].to(Circle(1.0))

trait JsonObjectEncoder[A] extends JsonEncoder[A] {
  def encode(value: A): JsonObject
}

def createObjectEncoder[A](fn: A => JsonObject): JsonObjectEncoder[A] =
  new JsonObjectEncoder[A] {
    def encode(value: A): JsonObject =
      fn(value)
  }

implicit val cnilObjectEncoder: JsonObjectEncoder[CNil] =
  createObjectEncoder(cnil => throw new Exception("Inconceivable!"))

implicit def coproductObjectEncoder[K <: Symbol, H, T <: Coproduct](
                                                                     implicit
                                                                     witness: Witness.Aux[K],
                                                                     hEncoder: Lazy[JsonEncoder[H]],
                                                                     tEncoder: JsonObjectEncoder[T]
                                                                   ): JsonObjectEncoder[FieldType[K, H] :+: T] = {
  val typeName = witness.value.name
  createObjectEncoder {
    case Inl(h) =>
      JsonObject(List(typeName -> hEncoder.value.encode(h)))
    case Inr(t) =>
      tEncoder.encode(t)
  }
}

val shape: Shape = Circle(1.0)

import chapter5.jsonvalue.encoder.Implicits._

implicit def genericObjectEncoder[A, H](
                                         implicit
                                         generic: LabelledGeneric.Aux[A, H],
                                         hEncoder: Lazy[JsonObjectEncoder[H]]
                                       ): JsonEncoder[A] =
  createObjectEncoder { value =>
    hEncoder.value.encode(generic.to(value))
  }

implicit val hnilEncoder: JsonObjectEncoder[HNil] =
  createObjectEncoder(hnil => JsonObject(Nil))

implicit def hlistObjectEncoder[K <: Symbol, H, T <: HList](
                                                             implicit
                                                             witness: Witness.Aux[K],
                                                             hEncoder: Lazy[JsonEncoder[H]],
                                                             tEncoder: JsonObjectEncoder[T]
                                                           ): JsonObjectEncoder[FieldType[K, H] :: T] = {
  val fieldName: String = witness.value.name
  createObjectEncoder { hlist =>
    val head = hEncoder.value.encode(hlist.head)
    val tail = tEncoder.encode(hlist.tail)
    JsonObject((fieldName, head) :: tail.fields)
  }
}

JsonEncoder[Shape].encode(shape)